﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using InputUtility;
using ParticlePlayground;

public class SantaController : MonoBehaviour {

	[SerializeField]
	float maxSpeed, acceleration;

	[SerializeField]
	GameObject frostParticle,treesParticleLeft,treesParticleRight,slegeMotionParticle,goldCoinCollectParticle,countDownTimer,HUD,gameOverScreen,santaMainObject,fireworks;

	[SerializeField]
	Animator reindeerAnimator,santaAnimator,ropeAnimator;

	[SerializeField]
	AudioSource bronzeGiftAudio,silverGiftAudio,goldGiftAudio,AudioManager;

	[SerializeField]
	GameObject[] raceLights,gameOverParticles,gameOverScreenBasedOnScore;

	[SerializeField]
	Image distanceMeter;

	[SerializeField]
	Text timerText,remainingTimeText,tensScoreText,hundredScoreText,thousandScoreText,finalScoreText;

	bool isSpawned,hasReached;

	public float[] LanesPositions;
	public float laneShiftSpeed;
	public float targetLanePosition, lanePosition;
	public GameObject iceBall,SCW;
	public Transform throwPeak;

	public enum PlayerLane
	{
		one,
		two,
		three
	};

	public PlayerLane currentLane, lastLane;

	string textContents;
	string[] lineByLine;

	private float velocity;

	int tens,hundreds,thousands,prevIndex;

	private Vector3 initialPosition;

	private ESantaState m_eCurrentState, m_ePrevState;

//	private Animator santaAnimator;

	int runState = Animator.StringToHash ("Base Layer.Run");

	private CharacterController _controller;

	// Use this for initialization
	void Start () 
	{
		
		StartCoroutine (HideFrostEffect (0.05f));
		_controller = GetComponent<CharacterController> ();
		SetState (ESantaState.Init);
		Messenger.AddListener<string> (Events.INPUT_RECIEVED, OnInputRecieved);
		Messenger.MarkAsPermanent(Events.INPUT_RECIEVED);
	}

	void OnDestroy()
	{
		Messenger.RemoveListener<string>(Events.INPUT_RECIEVED, OnInputRecieved);
	}

	void OnInputRecieved(string input)
	{
		if (input.CompareTo (InputActions.SWIPE_LEFT) == 0) 
		{
			LeftSideMoving ();
		} 
		else if (input.CompareTo (InputActions.SWIPE_RIGHT) == 0) 
		{
			RightSideMoving ();
		}
		else if (input.CompareTo (InputActions.FIRE) == 0) 
		{
			ThrowIceBall ();
		}

	}

	void Init()
	{
		initialPosition = transform.position;

		ScoreManager.score = 0;
		ScoreManager.noOfCollisions = 0;

		StartCoroutine (ShowTimer ());
		StartCoroutine (ReadTextFromFile ());
		slegeMotionParticle.SetActive (false);

		velocity = 0;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{

		switch (m_eCurrentState) 
		{
		case ESantaState.Init:
			break;

		case ESantaState.Run:

			distanceMeter.fillAmount = (transform.position.z / 2200);
			DisplayScore ();
			PlayerLaneChanging ();
			transform.Translate(Vector3.forward * velocity);
			transform.position = new Vector3 (lanePosition, transform.position.y, transform.position.z);
			break;

		case ESantaState.TimeOut:
			break;

		case ESantaState.ReachingSCW:
			transform.Translate(Vector3.forward * velocity);
			transform.position = new Vector3 (lanePosition, transform.position.y, transform.position.z);
			break;
		}

	}

	void Update()
	{

		switch (m_eCurrentState) 
		{
		case ESantaState.Init:
			break;

		case ESantaState.Run:

			if (velocity < maxSpeed) 
			{
				velocity = velocity + Time.deltaTime * acceleration;
			}
			else if (velocity >= maxSpeed)
			{
				velocity = maxSpeed;
				santaAnimator.SetTrigger ("Run");
				ropeAnimator.SetTrigger ("RopeContinue");
				reindeerAnimator.SetBool ("Run", true);
			}

			break;

		case ESantaState.TimeOut:
			break;

		case ESantaState.ReachingSCW:

			if (velocity < maxSpeed) {
				velocity = velocity + Time.deltaTime * acceleration;
			} else if (velocity >= maxSpeed) {
				velocity = maxSpeed;
				santaAnimator.SetTrigger ("Run");
				ropeAnimator.SetTrigger ("RopeContinue");
				reindeerAnimator.SetBool ("Run", true);
			}
			break;
		}

	}


	#region Lane Movement

	// Player lane changing here this method is called at fixed update Player state alive 
	void PlayerLaneChanging ()
	{
		switch (currentLane) {
		case PlayerLane.one:
			targetLanePosition = LanesPositions [0];
			break;
		case PlayerLane.two:
			targetLanePosition = LanesPositions [1];
			break;
		case PlayerLane.three:
			targetLanePosition = LanesPositions [2];
			break;
		}
		lastLane = currentLane;
		lanePosition = Mathf.Lerp (lanePosition, targetLanePosition, laneShiftSpeed);
	}


	void LeftSideMoving ()
	{

			if (m_eCurrentState == ESantaState.Run)
			{

				switch (currentLane)
				{
					case PlayerLane.one:
						break;
			
					case PlayerLane.two:
						currentLane = PlayerLane.one;
						santaAnimator.SetTrigger ("LeftMove");
						ropeAnimator.SetTrigger ("LeftRopeMove");
						break;
			
					case PlayerLane.three:
						currentLane = PlayerLane.two;
						santaAnimator.SetTrigger ("LeftMove");
						ropeAnimator.SetTrigger ("LeftRopeMove");
						break;
				}
		}
	}

	//Player Lane changes here to check the right side is any obstacle player hurt count increased

	void RightSideMoving ()
	{
			if (m_eCurrentState == ESantaState.Run) 
			{
				switch (currentLane) {
				case  PlayerLane.one:
					currentLane = PlayerLane.two;
					santaAnimator.SetTrigger ("RightMove");
					ropeAnimator.SetTrigger ("RightRopeMove");
					break;
				case  PlayerLane.two:
					currentLane = PlayerLane.three;
					santaAnimator.SetTrigger ("RightMove");
					ropeAnimator.SetTrigger ("RightRopeMove");
					break;
				case  PlayerLane.three:
					break;

				}
			}
	}
	#endregion


	#region Santa State Transitions
	void SetState(ESantaState newState)
	{
		if (m_eCurrentState != newState) 
		{
			m_ePrevState = m_eCurrentState;
			m_eCurrentState = newState;
			StateChange ();
		}
	}

	void StateChange()
	{
		switch (m_eCurrentState) 
		{
		case ESantaState.Init:
			Init ();
			break;

		case ESantaState.Run:
			Messenger.Broadcast<string> (Events.GAME_EVENT, GameEvents.GAME_IS_RUNNING);
			StartCoroutine (ShowRemainingTime ());
			break;

		case ESantaState.TimeOut:
			
			if (ScoreManager.score < int.Parse (lineByLine [3])) {
				santaMainObject.SetActive (false);
				Messenger.Broadcast<string> (Events.GAME_EVENT, GameEvents.GAME_OVER);
				gameOverScreen.SetActive (true);
				DisplayScore ();
			} 

			if (ScoreManager.score >= int.Parse(lineByLine[3]))
			{
				if(hasReached)
				{
					SetState (ESantaState.TimeOut);
				}
				else
				{
					SetState (ESantaState.ReachingSCW);
				}

			}
			break;
		}
	}
	#endregion


	#region Collision Delegates
	void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag (Constants.TAG_TREE_LEFT_OBS))
		{
			ScoreManager.noOfCollisions++;
//			Debug.Log ("Number of collisions: " + ScoreManager.noOfCollisions);
			other.gameObject.GetComponent<AudioSource> ().Play ();
			santaAnimator.SetTrigger ("Start");
			reindeerAnimator.SetBool ("Run", false);
			velocity -= velocity * 0.5f;
			CameraShake._instance.CameraShakeEffect ();
			treesParticleLeft.SetActive (true);
			StartCoroutine (MakeParticlesDisappear (treesParticleLeft));

		}
		else if (other.CompareTag (Constants.TAG_TREE_RIGHT_OBS))
		{
			ScoreManager.noOfCollisions++;
//			Debug.Log ("Number of collisions: " + ScoreManager.noOfCollisions);
			santaAnimator.SetTrigger ("Start");
			reindeerAnimator.SetBool ("Run", false);
			velocity -= velocity * 0.5f;

			CameraShake._instance.CameraShakeEffect ();
			other.gameObject.GetComponent<AudioSource> ().Play ();
			treesParticleRight.SetActive (true);
			StartCoroutine (MakeParticlesDisappear (treesParticleRight));
		}
		else if (other.CompareTag (Constants.TAG_SNOWMAN))
		{
			ScoreManager.noOfCollisions++;
//			Debug.Log ("Number of collisions: " + ScoreManager.noOfCollisions);
			other.gameObject.GetComponent<AudioSource> ().Play ();
			other.GetComponentInParent<PlaygroundParticlesC> ().enabled = true;
			StartCoroutine(ShowFrostEffect (0.1f, 0.6f));
			santaAnimator.SetTrigger ("Start");
			ropeAnimator.SetTrigger ("RopeStart");
			reindeerAnimator.SetBool ("Run", false);
			velocity -= velocity * 0.5f;
			StartCoroutine(TurnOffParticle(other));
		}
		else if (other.CompareTag (Constants.TAG_SNOWPILEOBS))
		{
			ScoreManager.noOfCollisions++;
//			Debug.Log ("Number of collisions: " + ScoreManager.noOfCollisions);
			other.gameObject.GetComponent<AudioSource> ().Play ();
			santaAnimator.SetTrigger ("Start");
			ropeAnimator.SetTrigger ("RopeStart");
			reindeerAnimator.SetBool ("Run", false);
			velocity -= velocity * 0.5f;
			other.GetComponentInChildren<ParticleSystem> ().Play ();
		}			

		else if (other.CompareTag (Constants.TAG_BRONZE_GIFTS))
		{
			ScoreManager.score += 10;
			bronzeGiftAudio.Play ();
			Destroy (other.gameObject);
		}
		else if (other.CompareTag (Constants.TAG_SILVER_GIFTS))
		{
			ScoreManager.score += 20;
			silverGiftAudio.Play ();
			Destroy (other.gameObject);
		}
		else if (other.CompareTag (Constants.TAG_GOLD_GIFTS))
		{
			ScoreManager.score += 100;
			goldGiftAudio.Play ();
			goldCoinCollectParticle.GetComponent<ParticleSystem> ().Play ();
			Destroy (other.gameObject);
		}
		if (other.CompareTag (Constants.TAG_REACHED_SCW))
		{
			StopRunningTowardsSCW ();
		}
		if (other.CompareTag (Constants.TAG_SPAWN))
		{
			Debug.Log (" Show 2222222222222222");
			ShowSCW ();
		}
	}

	#endregion

	public void ThrowIceBall()
	{
		if (m_eCurrentState != ESantaState.Run) 
		{
			return;
		}
		santaAnimator.SetTrigger ("HitBall");

		GameObject ib = GameObject.Instantiate (iceBall, throwPeak.position, Quaternion.identity) as GameObject;
		ib.GetComponent<AudioSource> ().Play ();
		ib.SetActive (true);
		ib.GetComponent<Rigidbody> ().AddForce (new Vector3 (0, -1, (30 + 50*velocity) ), ForceMode.Impulse);
	}

	void StopRunningTowardsSCW()
	{
		santaMainObject.SetActive (false);
		Messenger.Broadcast<string> (Events.GAME_EVENT, GameEvents.GAME_OVER);
		gameOverScreen.SetActive (true);
		DisplayScore ();
		fireworks.SetActive (true);
		StartCoroutine (ShowGameOverParticles ());
		hasReached = true;
		SetState (ESantaState.TimeOut);

	}

	void StopRunFunc()
	{
		SetState (ESantaState.TimeOut);
	}
		
	public void StartRun()
	{
		timerText.gameObject.SetActive (false);

		slegeMotionParticle.SetActive (true);

		santaAnimator.SetTrigger ("Start");
		ropeAnimator.SetTrigger ("RopeStart");

		reindeerAnimator.SetBool ("Walk",true);

		SetState (ESantaState.Run);
	}

	public void RestartGame()
	{
		Messenger.Broadcast<string> (Events.GAME_EVENT, GameEvents.GAME_RESTART);
	}

	IEnumerator HideFrostEffect(float decreasingSpeed)
	{
		while (frostParticle.GetComponent<FrostEffect> ().maxFrost > 0f) {
			frostParticle.GetComponent<FrostEffect> ().maxFrost -= decreasingSpeed;
				yield return null;
		}

	}

	IEnumerator ShowFrostEffect(float increasingSpeed, float targetMaxFrost)
	{
		while (frostParticle.GetComponent<FrostEffect> ().maxFrost < targetMaxFrost) {
			
			frostParticle.GetComponent<FrostEffect> ().maxFrost += increasingSpeed;
			yield return null;

		}
		StartCoroutine(HideFrostEffect (0.01f));
	}

	IEnumerator ShowTimer()
	{
		int timeLeft=3;

		while (timeLeft > 0) {
			if (timeLeft == 3) {
				timerText.text = "GET";
				timerText.color = new Color (255, 0, 0);
			}
			else if (timeLeft == 2) {
				timerText.text = "SET";
				timerText.color = new Color (255, 255, 0);
			}
			else if (timeLeft == 1) {
				timerText.text = "GO";
				timerText.color = new Color (0, 255, 0);
			}
			timeLeft -= 1;
			raceLights [timeLeft].SetActive (true);
			yield return new WaitForSeconds(1.0f);
			raceLights [timeLeft].SetActive (false);
		}
		countDownTimer.SetActive (false);
		HUD.SetActive (true);
		StartRun ();
	}

	IEnumerator MakeParticlesDisappear(GameObject targetParticle)
	{
		yield return new WaitForSeconds (1.0f);
		targetParticle.SetActive (false);
	}

	IEnumerator TurnOffParticle(Collider collision)
	{	
		yield return new WaitForSeconds (0.05f);
		collision.gameObject.GetComponentInParent<PlaygroundParticlesC> ().enabled = false;
		Destroy (collision.gameObject);

	}		

	IEnumerator ShowRemainingTime()
	{
		float timeLeft=Constants.GAME_TIME;

		while (timeLeft >= 0) {
			remainingTimeText.text = ((int) timeLeft).ToString();
			timeLeft -= 1;

			if ((ScoreManager.score >= int.Parse(lineByLine[3])) && !isSpawned) {
				Debug.Log ("Show SWC 1111111");
				ShowSCW ();
				isSpawned = true;
			}
			yield return new WaitForSeconds(1.0f);
		}
		StopRunFunc ();
	}

	IEnumerator ReadTextFromFile()
	{
		WWW data = new WWW ("file://" + Application.dataPath + "/StreamingAssets" + "/GameData.txt");

		yield return data;

		if (!string.IsNullOrEmpty (data.error)) {
			yield break;
		} 
		else {
			textContents = data.text;

			lineByLine = textContents.Split ('\n'); 
		}
		yield return null;

	}
	void ShowSCW()
	{
		SCW.transform.position = new Vector3 (-2.45f, -0.43f, transform.position.z + 120);
		SCW.SetActive (true);
		Invoke ("StopRunningTowardsSCW", 10.0f);
	}

	void DisplayScore()
	{
		thousands = ScoreManager.ConvertToThousandDigit ();
		hundreds = ScoreManager.ConvertToHundredDigit ();
		tens = ScoreManager.ConvertToTensDigit ();

		thousandScoreText.text = thousands.ToString ();
		hundredScoreText.text = hundreds.ToString ();
		tensScoreText.text = tens.ToString ();
	
		finalScoreText.text = ScoreManager.score.ToString ();

		if ((ScoreManager.score >= int.Parse (lineByLine [1])) && (ScoreManager.score <= int.Parse (lineByLine [2]))) {
			gameOverScreenBasedOnScore [0].SetActive (true);
			gameOverScreenBasedOnScore [1].SetActive (false);
			gameOverScreenBasedOnScore [2].SetActive (false);
			gameOverScreenBasedOnScore [3].SetActive (false);
		} else if ((ScoreManager.score >= int.Parse (lineByLine [2])) && (ScoreManager.score <= int.Parse (lineByLine [3]))) {
			gameOverScreenBasedOnScore [0].SetActive (false);
			gameOverScreenBasedOnScore [1].SetActive (true);
			gameOverScreenBasedOnScore [2].SetActive (false);
			gameOverScreenBasedOnScore [3].SetActive (false);
		} else if ((ScoreManager.score >= int.Parse (lineByLine [3]))) {
			gameOverScreenBasedOnScore [0].SetActive (false);
			gameOverScreenBasedOnScore [1].SetActive (false);
			gameOverScreenBasedOnScore [2].SetActive (true);
			gameOverScreenBasedOnScore [3].SetActive (false);
		} else {
			gameOverScreenBasedOnScore [0].SetActive (false);
			gameOverScreenBasedOnScore [1].SetActive (false);
			gameOverScreenBasedOnScore [2].SetActive (false);
			gameOverScreenBasedOnScore [3].SetActive (true);
		}
	}
	public enum ESantaState
	{
		Spawn = 0,
		Init,
		Run,
		TimeOut,
		Count,
		ReachingSCW
	}

	IEnumerator ShowGameOverParticles ()
	{
		AudioManager.volume = 0.1f;
		yield return new WaitForSeconds (0.5f);
		gameOverParticles [prevIndex].SetActive (false);
		int i = Random.Range (0, gameOverParticles.Length);

		if (prevIndex!=i)
			gameOverParticles [i].SetActive (true);
		prevIndex = i;
		StartCoroutine (ShowGameOverParticles ());
	} 
}
	

