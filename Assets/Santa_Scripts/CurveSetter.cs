﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using VacuumShaders.CurvedWorld ;
using InputUtility;

public class CurveSetter : MonoBehaviour
{

	public VacuumShaders.CurvedWorld.CurvedWorld_GlobalController curvedWorld;
	public float curveSpeedY, curveSpeedX ;
	public float upperLimitX, lowerLimitX;
	 
	public float[] initialCurvesY;
	public float[] initialCurvesX;

	bool isLeft, isUp,isRunning;
		void Start ()
		{
			
		Messenger.AddListener<string> (Events.GAME_EVENT, OnGameEventTrigger);

			curvedWorld._V_CW_Y_Bend_Size_GLOBAL = initialCurvesY[UnityEngine.Random.Range(0, initialCurvesY.Length)];
			curvedWorld._V_CW_X_Bend_Size_GLOBAL = initialCurvesY[UnityEngine.Random.Range(0, initialCurvesX.Length)];
			
		if (curvedWorld._V_CW_Y_Bend_Size_GLOBAL < 0)
				isLeft = false;
			else
				isLeft = true;

		if (curvedWorld._V_CW_X_Bend_Size_GLOBAL > 0)
			isUp = false;
		else
			isUp = true;
			
			StartCoroutine(BendToRightCurve ());

		}

 
		void Update ()
		{
		if (isRunning) {
			if (isLeft) {
				curvedWorld._V_CW_Y_Bend_Size_GLOBAL -= Time.deltaTime * curveSpeedY;
			} else {
				curvedWorld._V_CW_Y_Bend_Size_GLOBAL += Time.deltaTime * curveSpeedY;
			}

			if (isUp) {
				if (curvedWorld._V_CW_X_Bend_Size_GLOBAL < upperLimitX)
					curvedWorld._V_CW_X_Bend_Size_GLOBAL += Time.deltaTime * curveSpeedX;
				else
					curvedWorld._V_CW_X_Bend_Size_GLOBAL = upperLimitX;
			} else {
				if (curvedWorld._V_CW_X_Bend_Size_GLOBAL > lowerLimitX)
					curvedWorld._V_CW_X_Bend_Size_GLOBAL -= Time.deltaTime * curveSpeedX;
				else
					curvedWorld._V_CW_X_Bend_Size_GLOBAL = lowerLimitX;
			}
		}
		}

			void OnGameEventTrigger(string gameEvent)
			{
				if (gameEvent.CompareTo (GameEvents.GAME_IS_RUNNING) == 0) 
				{
					isRunning = true;
				}
				else if (gameEvent.CompareTo (GameEvents.GAME_OVER) == 0) 
				{
					isRunning = false;
				}
			}


		IEnumerator BendToRightCurve ()
		{
				yield return new WaitForSeconds (10);
				isLeft = !isLeft;
				StartCoroutine (BendToRightCurve ());
		} 

}
