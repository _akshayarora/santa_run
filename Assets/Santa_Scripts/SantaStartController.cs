﻿using UnityEngine;
using System.Collections;
using InputUtility;

public class SantaStartController : MonoBehaviour {


	public float speed = 10.0f;
	public GameObject[] checkpoints;
	public GameObject instructionCanvas;
	public GameObject sackIdle, sackInHand;

	[SerializeField]
	AudioSource merryChristmasAudio,magicalAudio,AudioManager;

	public Animator camAnimtor;
	public Transform finalPosition;

	[SerializeField]
	GameObject FrostParticle;

	int counter = 0;
	bool shallGo, isReachedRacePoint, hasTakenJump,isPlayedOnce;

	[SerializeField]
	float dist;

	Animator santaAnimator;

	[SerializeField]
	GameObject sackParticle;

	int idleState = Animator.StringToHash ("Base Layer.04_A_Santa_Idle_");
	int hiState = Animator.StringToHash ("Base Layer.01_A_Santa_ReturnNorthpole_talk");
	int idleSackState = Animator.StringToHash ("Base Layer.04_A_Santa_IdleSack_");
	int walkState = Animator.StringToHash ("Base Layer.02_B_Santa_walkSack_root");
	int jumpSackState = Animator.StringToHash ("Base Layer.05_A_Santa_jump_Sack");
	Vector3 direction;
	Quaternion toRotation;
	static Vector3 finalRotationVec = new Vector3(-7, 30, 0);
	static Vector3 lookRotationVec = new Vector3(0f, 185, 0);
Quaternion finalRotation = Quaternion.Euler(finalRotationVec);
//	Quaternion lookRotation = Quaternion.Euler(lookRotationVec);

	void Start()
	{
		Vector3 direction = checkpoints[checkpoints.Length-1].transform.position- transform.position;
		Quaternion toRotation = Quaternion.FromToRotation(transform.forward, direction);
//		chec
//		lookRotation = Quaternion.Euler(lookRotationVec);
	//	lookRotation = Quaternion.LookRotation(checkpoints[checkpoints.Length-1].transform.position);

		santaAnimator = GetComponent<Animator> ();
		if (!isPlayedOnce) {
			sackInHand.SetActive (false);
			sackIdle.SetActive (true);
			instructionCanvas.SetActive (true);
			santaAnimator.SetTrigger ("Start");
			Messenger.Broadcast<string> (Events.GAME_EVENT, GameEvents.START_ANIM);
			AudioManager.volume = 0.5f;
			merryChristmasAudio.Play ();
			isPlayedOnce = true;
		}

//		
//		instructionCanvas.SetActive (true);

		Messenger.AddListener<string> (Events.INPUT_RECIEVED, OnInputRecieved);
		Messenger.AddListener<string> (Events.GAME_EVENT, OnGameEventTrigger);

		Messenger.MarkAsPermanent (Events.INPUT_RECIEVED);
		Messenger.MarkAsPermanent (Events.GAME_EVENT);
	}

	void OnDestroy()
	{
		Messenger.RemoveListener<string>(Events.INPUT_RECIEVED, OnInputRecieved);
		Messenger.RemoveListener<string>(Events.GAME_EVENT, OnGameEventTrigger);
	}


	void OnInputRecieved(string input)
	{
		if (input.CompareTo (InputActions.START) == 0) 
		{
			CloseIntruction ();
		}
	}

	void OnGameEventTrigger(string gameEvent)
	{
	}

	void Update()
	{
		dist = Vector3.Distance (transform.position, checkpoints [counter].transform.position);

		if (!shallGo && !isReachedRacePoint && santaAnimator.GetCurrentAnimatorStateInfo (0).fullPathHash == walkState) {

			sackInHand.SetActive (true);
			sackParticle.SetActive (true);
			sackIdle.SetActive (false);
			AudioManager.volume = 1;
			shallGo = true;
			camAnimtor.enabled = true;
		}

		if (shallGo && !isReachedRacePoint) 
		{
			if (dist >= 0.9f) 
			{
				Move ();
			} 
			else 
			{
				
				if ((counter + 1) == (checkpoints.Length)) 
				{
					shallGo = false;
					isReachedRacePoint = true;
					counter = 0;
					SantaReachedRacePoint ();
				}
				else
				{
					counter++;
				}
			}
		}

		if (isReachedRacePoint  && !hasTakenJump && 
			santaAnimator.GetCurrentAnimatorStateInfo (0).fullPathHash == jumpSackState &&
			santaAnimator.GetCurrentAnimatorStateInfo (0).normalizedTime < 1f)
		{
			transform.rotation = Quaternion.Slerp(transform.rotation, finalRotation, Time.deltaTime * 10);
			transform.localPosition = Vector3.Lerp(transform.localPosition, finalPosition.localPosition, Time.deltaTime * 10);
		}
	}


	void SantaReachedRacePoint()
	{
		santaAnimator.SetTrigger ("Jump");
		StartCoroutine (LoadGame ());
	}

	IEnumerator LoadGame()
	{
		yield return new WaitForSeconds (0.3f);

		while (FrostParticle.GetComponent<FrostEffect> ().maxFrost < 1.0f) {
			FrostParticle.GetComponent<FrostEffect> ().maxFrost += 0.05f;
			yield return null;
		}
			Messenger.Broadcast<string> (Events.GAME_EVENT, GameEvents.GAME_START);

	}

	void Move()
	{

		Quaternion lookRotation = Quaternion.Euler(new Vector3(6.465f,163.477f,0));
		transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, speed * Time.deltaTime*2);

		transform.position += transform.forward * speed * Time.deltaTime;
	}

	public void CloseIntruction()
	{
		santaAnimator.SetTrigger ("Start");
		santaAnimator.SetTrigger ("Walk");
		merryChristmasAudio.Stop ();
		instructionCanvas.SetActive (false);
	}


}
