﻿using UnityEngine;
using System.Collections;
using InputUtility;

public class InputReceiver : InputControllerParent, IInputController {

	SantaController santaScript;

	bool hasInputTakenJoyStick = false;
	bool hasInputTakenDPAD = false;
	void Update () 
	{
	
	#if XBOX_CONTROLLER
	{
			
			if (Input.GetAxis ("Horizontal") < 0 && !hasInputTaken) {
				hasInputTaken = true;
				SwipeLeft ();
			}
			else if (Input.GetAxis ("D_Horizontal") < 0 && !hasInputTaken) {
				hasInputTaken = true;
				SwipeLeft ();
			} 
			else if (Input.GetAxis ("D_Horizontal") > 0 && !hasInputTaken) {
				hasInputTaken = true;
				SwipeRight ();
			}
			else if (Input.GetAxis ("Horizontal") > 0 && !hasInputTaken) {
				hasInputTaken = true;
				SwipeRight ();
			}
			else if (Input.GetKeyDown (KeyCode.Joystick1Button7)) {
				StartGame ();
			}
			else if (Input.GetKeyDown (KeyCode.Joystick1Button2)) {
				Fire ();
			}
			if (Input.GetAxis ("Horizontal") == 0) {
				hasInputTaken = false;
			}
				if (Input.GetAxis ("D_Horizontal") == 0) {
					hasInputTaken = false;
			}
	}
		#elif WIRELESS_CONTROLLER
		{

			if (Input.GetAxis ("LeftJoystickX") < 0 && !hasInputTakenDPAD) {
				Debug.Log("sdajkdsadsajfjafga");
				hasInputTakenDPAD = true;
				SwipeLeft ();
			} 
			else if (Input.GetAxis ("LeftJoystickX") > 0 && !hasInputTakenDPAD) {
				hasInputTakenDPAD = true;
				SwipeRight ();
			}
			else if (Input.GetAxis ("D_Horizontal") < 0 && !hasInputTakenJoyStick) {
				hasInputTakenJoyStick = true;
				SwipeLeft ();
			} 
			else if (Input.GetAxis ("D_Horizontal") > 0 && !hasInputTakenJoyStick) {
				hasInputTakenJoyStick = true;
				SwipeRight ();
			}
			else if (Input.GetKeyDown (KeyCode.JoystickButton9)) {
				Debug.Log("Input");
				StartGame ();
			}
			else if (Input.GetKeyDown (KeyCode.JoystickButton0)) {
				Fire ();
			}
			if (Input.GetAxis ("LeftJoystickX") == 0) {
				hasInputTakenDPAD = false;
			}
			if (Input.GetAxis ("D_Horizontal") == 0) {
				hasInputTakenJoyStick = false;
			}
		}
	
	#else
	{
		if (Input.GetKeyDown (KeyCode.RightArrow)) 
		{
		SwipeRight ();
		}

		if (Input.GetKeyDown (KeyCode.LeftArrow)) 
		{
		SwipeLeft ();
		}

		if (Input.GetKeyDown (KeyCode.Space)) 
		{
		Fire ();
		}

		if (Input.GetKeyDown (KeyCode.Return)) 
		{
		StartGame ();
		}
	}
		#endif
	}
}
