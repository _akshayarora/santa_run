﻿using System;
using UnityEngine;

namespace InputUtility
{
	public class InputControllerParent : MonoBehaviour
	{
		public void SwipeLeft(){
			Messenger.Broadcast <string> (Events.INPUT_RECIEVED,InputActions.SWIPE_LEFT);
		}

		public void SwipeRight(){
			Messenger.Broadcast <string> (Events.INPUT_RECIEVED,InputActions.SWIPE_RIGHT);
		}
		public void SwipeUp(){
			Messenger.Broadcast <string> (Events.INPUT_RECIEVED,InputActions.SWIPE_UP);
		}
		public void SwipeDown(){
			Messenger.Broadcast <string> (Events.INPUT_RECIEVED,InputActions.SWIPE_DOWN);
		}

		public void StartGame()
		{
			Messenger.Broadcast <string> (Events.INPUT_RECIEVED,InputActions.START);
		}

		public void Fire(){
			Messenger.Broadcast <string> (Events.INPUT_RECIEVED,InputActions.FIRE);
		}

	}
}

