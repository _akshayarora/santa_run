﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	private Transform lookAt;
	private Vector3 startOffset, newPos;

	// Use this for initialization
	void Start () 
	{
		lookAt = GameObject.FindGameObjectWithTag (Constants.TAG_SANTA).transform;
		startOffset = transform.position - lookAt.position;
	}
	
	// Update is called once per frame
	void Update () 
	{
		newPos = lookAt.position + startOffset;
		newPos.x = 0;
		transform.position = newPos;
	}
}
