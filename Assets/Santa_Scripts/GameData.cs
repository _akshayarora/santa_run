﻿using UnityEngine;
using System.Collections;
using System;

[System.Serializable]
public class GameData 
{
	public DateTime activationTime;
	public bool isValid;
}
