﻿using UnityEngine;
using System.Collections;

public static class ScoreManager {

	public static int score;
	public static int noOfCollisions;


	public static int ConvertToThousandDigit()
	{
		if (score >= 1000) {
			int num = score / 1000;
			return num;
		} else {
			return 0;
		}

	}

	public static int ConvertToHundredDigit()
	{
		if (score >= 1000) {
			int num = (score / 100)%10;
			return num;
		}
		else if (score >= 100) {
			int num = score / 100;
			return num;
		} else {
			return 0;
		}

	}

	public static int ConvertToTensDigit()
	{
		if (score >= 100) {
			int num = (score / 10) % 10;
			return num;
		} else {
			int num = score / 10;
			return num;
		}
			
	}
		


}
