﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {

	#region Collision Delegates
	void OnCollisionEnter(Collision collision)
	{
		if (collision.collider.CompareTag (Constants.TAG_ICEBALL))
		{
			Destroy (collision.gameObject);
		}
	}
	#endregion
}
