﻿using UnityEditor;
using UnityEngine;

public class TerrainTools : EditorWindow
{
	[MenuItem("Tools/Terrain Tools")]
	static void Iinit(MenuCommand command)
	{
		TerrainTools i = (TerrainTools)ScriptableObject.CreateInstance(typeof(TerrainTools));
		i.ShowUtility();
	}


	void OnGUI()
	{

		Terrain ter = Terrain.activeTerrain;
		GUILayout.Label("Terriain Name: " + ter.name, EditorStyles.boldLabel);

		if(GUILayout.Button("Store Terrain Data"))
			StoreTerrainData();

		if(GUILayout.Button("Set Terrain Data"))
			SetTerrainData();

		if(GUILayout.Button("Reset Terrain Data"))
			ResetTerrain();

		GUILayout.Space(10);

		GUILayout.BeginHorizontal();
		GUILayout.Label("Currently Stored Data ", EditorStyles.boldLabel);
		GUILayout.Label(stored);
		GUILayout.EndHorizontal();

		GUILayout.BeginHorizontal();
		GUILayout.Label("Currently Selected ", EditorStyles.boldLabel);
		GUILayout.Label(Terrain.activeTerrain.terrainData.name);
		GUILayout.EndHorizontal();
	}

	void ResetTerrain()
	{
		Terrain.activeTerrain.terrainData = new TerrainData();
	}

	TerrainData terData = new TerrainData();
	string stored = "";
	void StoreTerrainData()
	{
		stored = Terrain.activeTerrain.terrainData.name;
		terData = Terrain.activeTerrain.terrainData;
	}

	void SetTerrainData()
	{
		if(terData != null)
		{
			TerrainData newTerrainData = (TerrainData) Object.Instantiate(terData);
			Terrain.activeTerrain.terrainData = newTerrainData; 
			TerrainCollider tc = Terrain.activeTerrain.gameObject.GetComponent<TerrainCollider>();
			tc.terrainData = newTerrainData;
		}
	}
} 
